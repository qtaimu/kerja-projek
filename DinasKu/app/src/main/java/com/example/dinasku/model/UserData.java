package com.example.dinasku.model;

public class UserData {

    private String NM_PEGAWAI;
    private String ALAMAT;
    private String TELEPON;

    public String getNM_PEGAWAI() {
        return NM_PEGAWAI;
    }

    public void setNM_PEGAWAI(String NM_PEGAWAI) {
        this.NM_PEGAWAI = NM_PEGAWAI;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    public String getTELEPON() {
        return TELEPON;
    }

    public void setTELEPON(String TELEPON) {
        this.TELEPON = TELEPON;
    }
}
