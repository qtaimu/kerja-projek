package com.example.dinasku;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.dinasku.base.BaseActivity;
import com.example.dinasku.config.AppConfig;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        super.setHideActionBar();
        splash();
    }

    private void splash()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        }, AppConfig.SPLASHSCREEN_DELAY);
    }

}
