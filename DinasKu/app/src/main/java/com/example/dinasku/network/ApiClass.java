package com.example.dinasku.network;

import com.example.dinasku.config.ClientConfig;
import com.example.dinasku.model.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiClass {

    @FormUrlEncoded
    @POST(ClientConfig.api_login_url)
    Call<User> login(
            @Field("NIP")
    )


}
