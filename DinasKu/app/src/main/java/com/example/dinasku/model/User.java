package com.example.dinasku.model;

import com.example.dinasku.base.BaseResponse;

public class User extends BaseResponse {

    private UserData data;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }
}
